package ua.com.test.ProjEventListener;


import com.atlassian.event.api.EventListener;
import com.atlassian.event.api.EventPublisher;
import com.atlassian.jira.bc.issue.IssueService;
import com.atlassian.jira.component.ComponentAccessor;
import com.atlassian.jira.config.SubTaskManager;
import com.atlassian.jira.event.ProjectCreatedEvent;
import com.atlassian.jira.exception.CreateException;
import com.atlassian.jira.issue.IssueInputParameters;
import com.atlassian.jira.issue.MutableIssue;
import com.atlassian.jira.issue.issuetype.IssueType;
import com.atlassian.jira.project.Project;
import com.atlassian.jira.user.ApplicationUser;
import com.atlassian.plugin.spring.scanner.annotation.export.ExportAsDevService;
import com.atlassian.plugin.spring.scanner.annotation.imports.ComponentImport;
import org.apache.log4j.Logger;
import org.springframework.beans.factory.DisposableBean;
import org.springframework.beans.factory.InitializingBean;
import org.springframework.beans.factory.annotation.Autowired;

import java.util.Iterator;
import java.util.Map;

@ExportAsDevService
public class ProjListener implements InitializingBean, DisposableBean  {



    private final EventPublisher eventPublisher;
    private final IssueService issueService;
    private static final Logger log = Logger.getLogger(ProjListener.class);

    @Autowired
    public ProjListener(@ComponentImport EventPublisher eventPublisher,
                        @ComponentImport IssueService issueService) {
        this.eventPublisher = eventPublisher;
        this.issueService = issueService;
    }

    public void destroy() throws Exception {

        eventPublisher.unregister(this);

    }

    public void afterPropertiesSet() throws Exception {
        eventPublisher.register(this);
    }

    @EventListener
    public void onProjectEvent(ProjectCreatedEvent event){
        System.out.println(">>>>>>>>> Project Event work!");
        Project project = event.getProject();
        Iterator<IssueType> iterator = project.getIssueTypes().iterator();
        IssueType task = iterator.next();
        IssueType sub = iterator.next();
        System.out.println("?>>>>>>>>>>> project = " + project);

        ApplicationUser user = project.getProjectLead();

        IssueInputParameters issueFirstParameters = issueService.newIssueInputParameters();
        issueFirstParameters.setSummary("Meet!!!");
        issueFirstParameters.setDescription("MeetMeet...");
        issueFirstParameters.setProjectId(project.getId());
        issueFirstParameters.setPriorityId("3");
        issueFirstParameters.setIssueTypeId(task.getId());
        issueFirstParameters.setReporterId(user.getKey());



        IssueInputParameters issueSecParameters = issueService.newIssueInputParameters();
        issueSecParameters.setSummary("To negotiate with the client!!!");
        issueSecParameters.setDescription("To negotiate with the client!...");
        issueSecParameters.setProjectId(project.getId());
        issueSecParameters.setPriorityId("3");
        issueSecParameters.setIssueTypeId(sub.getId());
        issueSecParameters.setReporterId(user.getKey());

        IssueInputParameters issueThirParameters = issueService.newIssueInputParameters();
        issueThirParameters.setSummary("To have a deal!!!");
        issueThirParameters.setDescription("To have a deal!!!...");
        issueThirParameters.setProjectId(project.getId());
        issueThirParameters.setPriorityId("3");
        issueThirParameters.setIssueTypeId(sub.getId());
        issueThirParameters.setReporterId(user.getKey());

        IssueService.IssueResult issueResult = null;
        IssueService.CreateValidationResult result = issueService.validateCreate(user, issueFirstParameters);
        if(!result.getErrorCollection().hasAnyErrors()){
            issueResult = issueService.create(user, result);
        }else{
            Map<String, String> errors = result.getErrorCollection().getErrors();
            for(Map.Entry<String,String> entry: errors.entrySet()){
                System.out.println(">>>> ERROR >>>> " + entry.getKey() + " - " + entry.getValue());
            }

        }

        if(issueResult != null) {
            MutableIssue mutableIssue = issueResult.getIssue();


            SubTaskManager subTaskManager = ComponentAccessor.getSubTaskManager();
            result = issueService.validateSubTaskCreate(user, mutableIssue.getId(), issueSecParameters);
            if (!result.getErrorCollection().hasAnyErrors()) {
                issueResult = issueService.create(user, result);
                try {
                    subTaskManager.createSubTaskIssueLink(mutableIssue, issueResult.getIssue(), user);
                } catch (CreateException e) {
                    System.err.println("e" + e);
                    e.printStackTrace();
                }
            } else {
                Map<String, String> errors = result.getErrorCollection().getErrors();
                for (Map.Entry<String, String> entry : errors.entrySet()) {
                    System.out.println(">>>> ERROR >>>> " + entry.getKey() + " - " + entry.getValue());
                }

            }

            result = issueService.validateSubTaskCreate(user, mutableIssue.getId(), issueThirParameters);
            if (!result.getErrorCollection().hasAnyErrors()) {
                issueResult = issueService.create(user, result);
                try {
                    subTaskManager.createSubTaskIssueLink(mutableIssue, issueResult.getIssue(), user);
                } catch (CreateException e) {
                    System.err.println("e" + e);
                }
            } else {
                Map<String, String> errors = result.getErrorCollection().getErrors();
                for (Map.Entry<String, String> entry : errors.entrySet()) {
                    System.out.println(">>>> ERROR >>>> " + entry.getKey() + " - " + entry.getValue());
                }

            }
        }


    }
}
